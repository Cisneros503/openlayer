

//EJEMPLOS DE FUNCIONES PARA TRABAJAR EN OPENLAYERS SACADOS DEL SIGUIENTE LINK OFICIAL DE OL
//https://openlayers.org/workshop/en/vector/style.html



import 'ol/ol.css';
import GeoJSON from 'ol/format/GeoJSON';
import Map from 'ol/Map';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import View from 'ol/View';
import sync from 'ol-hashed';
import DragAndDrop from 'ol/interaction/DragAndDrop';
import Modify from 'ol/interaction/Modify';
import Draw from 'ol/interaction/Draw';
import Snap from 'ol/interaction/Snap';
import {Style, Fill, Stroke} from 'ol/style';



const map = new Map({
  target: 'map-container',
  layers: [
    new VectorLayer({
      source: new VectorSource({
        format: new GeoJSON(),
        url: './data/countries.json'
      })
    })
  ],
  view: new View({
    center: [0, 0],
    zoom: 2
  })
});


sync(map);

const source = new VectorSource();

const layer = new VectorLayer({
  source: source,
  style: new Style({
    fill: new Fill({
      color: 'red'
    }),
    stroke: new Stroke({
      color: 'white'
    })
  })
});



map.addLayer(layer);

// permite arrastrar e interactuar coon los features
map.addInteraction(new DragAndDrop({
  source: source,
  formatConstructors: [GeoJSON]
}));

//permite dibujar poligonos
map.addInteraction(new Draw({
  type: 'Polygon',
  source: source
}));

//permite modificar poligons map
map.addInteraction(new Snap({
  source: source
}));

//borrar los sources dependiendo del id
const clear = document.getElementById('clear');
clear.addEventListener('click', function() {
  source.clear();
});

//serializar features para descargar la data
const format = new GeoJSON({featureProjection: 'EPSG:3857'});
const download = document.getElementById('download');
source.on('change', function() {
  const features = source.getFeatures();
  const json = format.writeFeatures(features);
  download.href = 'data:text/json;charset=utf-8,' + json;
});












//permite interactuar con los poligons
map.addInteraction(new Modify({
  source: source
}));



//////AVANCE MAKING IT LOOK NICE
